import express from "express";
import { connectDB } from "./config/db";

import { UserRouter } from "./api/route/user.route";

connectDB();

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept,x-auth"
  );
  next();
});

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get("/", (req, res) =>
  res.json({ msg: "Horus will fix this world !! and wipe out human plague!!!" })
);

app.use("/api/user", UserRouter);

const port = process.env.PORT || 4377;

app.listen(port, () => {
  console.log(`==> Broadcasting at port: ${port}`);
});
