import express from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import User from "../model/User";
import { validateRegisterInput } from "../../util/data-validator";

export const addNewUser = async (req: any, res: any, next: any) => {
  let { username, email, password } = req.body;

  let errors = validateRegisterInput(req.body);
  console.log(errors);
  if (errors.length !== 0) return res.status(400).json({ errors: errors });

  try {
    let user = await User.findOne({ email });

    if (user) {
      return res
        .status(400)
        .json({ type: "Validation", msg: "User already exists" });
    }

    bcrypt.genSalt(10, async (err, salt) => {
      bcrypt.hash(password, salt, async (err, hash) => {
        password = hash;

        const newUser = new User({ username, email, password });

        await newUser.save();

        res.status(200).json(newUser);
      });
    });
  } catch (error) {
    console.log(error.message);

    res.status(500).send("Server Error");
  }
};
