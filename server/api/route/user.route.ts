import express from "express";
// import auth from "../auth/auth";
import { addNewUser } from "../controller/user.controller";

const UserRouter = express.Router();

UserRouter.post("/add-user", addNewUser);
// UserRouter.post("/login-user", loginUser);
// UserRouter.get("/all", getAllUsers);
// UserRouter.get("/:id", auth, getUserById);

export { UserRouter };
