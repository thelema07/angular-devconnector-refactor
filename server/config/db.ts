import mongoose from "mongoose";
import { dbConfig } from "./dbConfig";

export const connectDB = async () => {
  try {
    await mongoose.connect(dbConfig.mongoUri, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: true,
      useUnifiedTopology: true
    });
    console.log("==> Higher powers spread data from MongoDB");
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};
