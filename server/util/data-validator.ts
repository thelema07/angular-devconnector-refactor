import validator from "validator";
import { isEmpty } from "./isEmpty";

export const validateRegisterInput = (data: any): any => {
  let errors = [];

  if (!validator.isEmail(data.email)) {
    errors.push({
      type: "Validation",
      msg: "Email Invalid, Please add a valid email"
    });
  }

  if (validator.isEmpty(data.email)) {
    errors.push({ type: "Validation", msg: "Email Required" });
  }

  if (!validator.isLength(data.username, { min: 4, max: 21 })) {
    errors.push({
      type: "Validation",
      msg: "Usename must be between 4 and 21 characters"
    });
  }

  if (validator.isEmpty(data.username)) {
    errors.push({
      type: "Validation",
      msg: "Usename required"
    });
  }

  if (!validator.isLength(data.password, { min: 7, max: 21 })) {
    errors.push({
      type: "Validation",
      msg: "Password should have minimum of 7 characters"
    });
  }

  if (validator.isEmpty(data.password)) {
    errors.push({
      type: "Validation",
      msg: "Password required"
    });
  }

  if (!validator.isLength(data.password2, { min: 7, max: 21 })) {
    errors.push({
      type: "Validation",
      msg: "Password confirmation should have minimum of 7 characters"
    });
  }

  if (validator.isEmpty(data.password2)) {
    errors.push({
      type: "Validation",
      msg: "Password confirmation required"
    });
  }

  if (!validator.equals(data.password, data.password2)) {
    errors.push({
      type: "Validation",
      msg: "Passwords must match"
    });
  }

  return errors;
};
