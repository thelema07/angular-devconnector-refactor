import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class UserService {
  baseUrl = environment.apiUrl + "user/";

  constructor(private http: HttpClient) {}

  registerUser(data: any) {
    return this.http.post(this.baseUrl + "add-user", data);
  }
}
