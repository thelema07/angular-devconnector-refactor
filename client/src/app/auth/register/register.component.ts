import { Component, OnInit, TemplateRef } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";

import { UserService } from "../../services/user-service/user.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"]
})
export class RegisterComponent implements OnInit {
  modalRef: BsModalRef;
  data: any = {};
  email: string;
  username: string;
  password: string;
  password2: string;

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private modalService: BsModalService
  ) {}

  ngOnInit() {}

  addUser(template: TemplateRef<any>) {
    const data = {
      email: this.email,
      username: this.username,
      password: this.password,
      password2: this.password2
    };

    this.userService.registerUser(data).subscribe(
      res => {
        console.log(res);
        this.openModal(template);
      },
      error => {
        this.openModal(template);
      }
    );
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}
