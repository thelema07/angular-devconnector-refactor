import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

import { UserService } from "./services/user-service/user.service";

import { AppRoutingModule } from "./routing/app-routing.module";
import { BootstrapModule } from "./styling/bootstrap.module";
import { AppComponent } from "./app.component";
import { NavbarComponent } from "./layout/navbar/navbar.component";
import { LandingComponent } from "./layout/landing/landing.component";
import { LoginComponent } from "./auth/login/login.component";
import { RegisterComponent } from "./auth/register/register.component";
import { FooterComponent } from "./layout/footer/footer.component";

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LandingComponent,
    LoginComponent,
    RegisterComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    BootstrapModule
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule {}
